// Upd 28.11


$('.jumbotron-slider').slick({
    arrows: true,
    autoplay: false,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>'
});


$(function() {

    $('.filter-value').on('click touchstart', function(e){
        e.preventDefault();
        $('.blog-filter').toggleClass('open');
    });

    $('.filter-drop li a').on('click touchstart', function(e){
        e.preventDefault();
        var txt = $(this).text();
        $('.filter-drop').find('li').removeClass('active');
        $(this).closest('li').addClass('active');
        $('.filter-value span').text(txt);
        $('.blog-filter').removeClass('open');
    });


});


$('.blog-sidebar-toggle').on('click touchstart', function(e){
    e.preventDefault();
    $('.blog-sidebar').toggleClass('open');
});

