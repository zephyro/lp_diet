// Navs

$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(100);
        pull.toggleClass('open');
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 992 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});


$(function() {
    var spull = $('.nav-toggle');
    var smenu = $(spull.attr("data-target"));

    $(spull).on('click', function(e) {
        e.preventDefault();
        smenu.toggleClass('open');
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 992 && smenu.is(':hidden')) {
            smenu.removeAttr('style');
        }
    });
});



// Модальные окнка

$(".btn-modal").fancybox({
    'padding' : 0,
    'tpl'        : {
        wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner fancybox-scroll"></div></div></div></div>'
    }
});

$(".btn-modal-no-close").fancybox({
    'padding' : 0,
    'closeBtn': false,
    'tpl'        : {
        wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin fancybox-skin-adv"><div class="fancybox-outer"><div class="fancybox-inner fancybox-scroll"></div></div></div></div>'
    }
});


$(".btn-modal-video").fancybox({
    'padding'    : 0
});


$("input[type='radio']").ionCheckRadio();
$("input[type='checkbox']").ionCheckRadio();


// Отзывы

$('.review-slider').slick({
    arrows: false,
    autoplay: false,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1
});

//tabs


$('.tabs-nav a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');

    console.log(tab);
    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});

// Сочетания продуктов

$('.products-title').click(function(e) {
    e.preventDefault();
    $(this).closest('ul').toggleClass('open');
});


// Input[number]

$(document).ready(function() {
    $('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
});


// FAQ

$('.faq-question').click(function(e) {
    e.preventDefault();
    $(this).closest('li').toggleClass('open');
});


// Diary

$('.diary-nav-single a').click(function(e) {
    e.preventDefault();
    var box =  $(this).closest('.diary-nav');
    box.find('li').removeClass('active');
    $(this).closest('li').addClass('active');

});

$('.diary-nav-multi a').click(function(e) {
    e.preventDefault();
    $(this).closest('li').toggleClass('active');

});

$('.diary-nav-dropdown a').click(function(e) {
    $(this).closest('.dropdown').removeClass('open');

});


$('.dropdown').hover(function(e)  {
    e.preventDefault();
    $(this).addClass('open')

});

$( ".dropdown" ).mouseleave(function(e) {
    e.preventDefault();
    $(this).removeClass('open')
});


/// Всплывающие подсказки

$( document ).ready( function()
{
    var targets = $( '[rel~=tooltip]' ),
        target  = false,
        tooltip = false,
        title   = false;

    targets.bind( 'mouseenter', function()
    {
        target  = $( this );
        tip     = target.attr( 'title' );
        tooltip = $( '<div id="tooltip"></div>' );

        if( !tip || tip == '' )
            return false;

        target.removeAttr( 'title' );
        tooltip.css( 'opacity', 0 )
            .html( tip )
            .appendTo( 'body' );

        var init_tooltip = function()
        {
            if( $( window ).width() < tooltip.outerWidth() * 1.5 )
                tooltip.css( 'max-width', $( window ).width() / 2 );
            else
                tooltip.css( 'max-width', 370 );

            var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                pos_top  = target.offset().top - tooltip.outerHeight() - 25;

            if( pos_left < 0 )
            {
                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                tooltip.addClass( 'left' );
            }
            else
                tooltip.removeClass( 'left' );

            if( pos_left + tooltip.outerWidth() > $( window ).width() )
            {
                pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                tooltip.addClass( 'right' );
            }
            else
                tooltip.removeClass( 'right' );

            if( pos_top < 0 )
            {
                var pos_top  = target.offset().top + target.outerHeight();
                tooltip.addClass( 'top' );
            }
            else
                tooltip.removeClass( 'top' );

            tooltip.css( { left: pos_left, top: pos_top } )
                .animate( { top: '-=7', opacity: 1 }, 50 );
        };

        init_tooltip();
        $( window ).resize( init_tooltip );

        var remove_tooltip = function()
        {
            tooltip.animate( { top: '-=7', opacity: 0 }, 50, function()
            {
                $( this ).remove();
            });

            target.attr( 'title', tip );
        };

        target.bind( 'mouseleave', remove_tooltip );
        tooltip.bind( 'click', remove_tooltip );
    });
});


/// Дата и время

$('.input-time').datetimepicker({
    lang:'ru',
    step:15,
    datepicker:false,
    format:'H:i'
});


//Analysis


$('.nav-block > li > a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tab-container');

    console.log(tab);
    console.log(box);
    $(this).closest('.nav-block').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.addClass('dddd');

    box.find('> .tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});

// LK_Curator_Single_Member

$('.member-nav > li > a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.member-profile');

    console.log(tab);
    console.log(box);
    $(this).closest('.member-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');


    box.find('.member-item').removeClass('active');
    box.find(tab).addClass('active');
});

